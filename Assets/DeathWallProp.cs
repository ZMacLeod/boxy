﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWallProp : MonoBehaviour
{
    public Vector3 startPos;
    public float scale;
    public Vector3 rotAmount;


    // Start is called before the first frame update
    void Start()
    {
        scale = Random.Range(1f, 2f);
        transform.localScale = new Vector3(scale, scale, scale);
        transform.localRotation = Quaternion.Euler(Random.Range(0, 360f), Random.Range(0, 360f), Random.Range(0, 360f));
      rotAmount = new Vector3(Random.Range(-45f, 45f), Random.Range(-45f, 45f), Random.Range(-45f, 45f));
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(scale,scale,scale);
        transform.localPosition = startPos + new Vector3(Random.value, Random.value, Random.value) * 0.05f;
        transform.Rotate(rotAmount * Time.deltaTime, Space.World);
      
    }

   
}
