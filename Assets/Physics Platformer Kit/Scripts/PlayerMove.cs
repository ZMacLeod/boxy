﻿using UnityEngine;
using System.Collections;

//handles player movement, utilising the CharacterMotor class
[RequireComponent(typeof(CharacterMotor))]
[RequireComponent(typeof(DealDamage))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerMove : MonoBehaviour
{

    public static PlayerMove get;

    public Vector3 lastSafeLocation;

    //setup
    public bool sidescroller;                   //if true, won't apply vertical input
    public Transform mainCam, floorChecks;      //main camera, and floorChecks object. FloorChecks are raycasted down from to check the player is grounded.
    public Animator animator;                   //object with animation controller on, which you want to animate
    public AudioClip jumpSound;                 //play when jumping
    public AudioClip landSound;                 //play when landing on ground

    //movement
    public float accel = 70f;                   //acceleration/deceleration in air or on the ground
    public float airAccel = 18f;
    public float decel = 7.6f;
    public float airDecel = 1.1f;
    [Range(0f, 5f)]
    public float rotateSpeed = 0.7f, airRotateSpeed = 0.4f; //how fast to rotate on the ground, how fast to rotate in the air
    public float maxSpeed = 9;                              //maximum speed of movement in X/Z axis
    public float slopeLimit = 40, slideAmount = 35;         //maximum angle of slopes you can walk on, how fast to slide down slopes you can't
    public float movingPlatformFriction = 7.7f;             //you'll need to tweak this to get the player to stay on moving platforms properly

    public float wallRunMaxCD;
    public float wallRunCD;

    public float minWallRunSpeed; // how fast upwards you have to be moving in order to wallrun
    public float wallRunHeight;
    public float wallRunApex;


    public float jumpHeight;
    public float jumpApexTime;

    public float shorthopHeight;
    public float shorthopApexTime;

    public bool shorthop;

    public Vector3 wallPushDir;

    private float wallJumpCoyote;

    //jumping

    public float wallRunForce { get { return wallRunHeight / wallRunApex * 2f; } }
    public float wallRunGravity { get { return wallRunForce / wallRunApex; } }

    public float wallJumpHor; // horizontal velocity from a walljump
    public float wallJumpHeight;

    public float wallJumpVer { get { return Mathf.Sqrt(2 * gravityScale * wallJumpHeight); } }

    public float wallJumpAirAccel;
    public float wallJumpTime;
    private float curWallJumpTime;

    public Vector3 jumpForce { get { return new Vector3(0f, jumpHeight / jumpApexTime * 2f, 0f); } }
    public float shorthopSpeed { get { return shorthopHeight / shorthopApexTime * 2f; } }
    public float shorthopGravity { get { return shorthopSpeed / shorthopApexTime; } }
    public float gravityScale { get { return jumpForce.y / jumpApexTime; } }
    public float terminalVelocity;
    public float wallSlideTerminalVelocity;


    public float jumpLeniancy = 0.17f;                      //how early before hitting the ground you can press jump, and still have it work
    [HideInInspector]
    public int onEnemyBounce;

    private bool wallPush; // is pushing into a wall


    private bool grounded;
    private bool wallJumped;
    private Transform[] floorCheckers;
    private Quaternion screenMovementSpace;
    private float airPressTime, groundedCount, curAccel, curDecel, curRotateSpeed, slope;
    private Vector3 direction, moveDirection, screenMovementForward, screenMovementRight, movingObjSpeed;

    private CharacterMotor characterMotor;
    private EnemyAI enemyAI;
    private DealDamage dealDamage;
    private Rigidbody rigid;
    private AudioSource aSource;



    //setup
    void Awake()
    {
        //create single floorcheck in centre of object, if none are assigned
        if (!floorChecks)
        {
            floorChecks = new GameObject().transform;
            floorChecks.name = "FloorChecks";
            floorChecks.parent = transform;
            floorChecks.position = transform.position;
            GameObject check = new GameObject();
            check.name = "Check1";
            check.transform.parent = floorChecks;
            check.transform.position = transform.position;
            Debug.LogWarning("No 'floorChecks' assigned to PlayerMove script, so a single floorcheck has been created", floorChecks);
        }
        //assign player tag if not already
        if (tag != "Player")
        {
            tag = "Player";
            Debug.LogWarning("PlayerMove script assigned to object without the tag 'Player', tag has been assigned automatically", transform);
        }

        get = this;

        //usual setup
        mainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
    
        dealDamage = GetComponent<DealDamage>();
        characterMotor = GetComponent<CharacterMotor>();
        rigid = GetComponent<Rigidbody>();
        aSource = GetComponent<AudioSource>();
        //gets child objects of floorcheckers, and puts them in an array
        //later these are used to raycast downward and see if we are on the ground
        floorCheckers = new Transform[floorChecks.childCount];
        for (int i = 0; i < floorCheckers.Length; i++)
            floorCheckers[i] = floorChecks.GetChild(i);
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered?");
        TriggerZone trigger = other.GetComponent<TriggerZone>();
        if (trigger != null) { trigger.Activate(); }
    }


    void WallRunCalculations()
    {
        if (!wallPush)
        {
            if (IsWallPushing() && rigid.velocity.y >= minWallRunSpeed && wallRunCD <= 0)
            {
                Debug.Log("Begin Wallrun");

                wallPush = true;
                wallRunCD = wallRunMaxCD;
                rigid.velocity = new Vector3(rigid.velocity.x, wallRunForce, rigid.velocity.y);
            }

        }
        if (wallPush)
        {

            wallRunCD = wallRunMaxCD;
            rigid.velocity = new Vector3(
                rigid.velocity.x,
                    Mathf.Max(-wallSlideTerminalVelocity, rigid.velocity.y - (wallRunGravity * Time.deltaTime)),

                    rigid.velocity.z);
            if (rigid.velocity.y >= 0)
            {
                wallPush = IsWallPushing();
            }
            else
            {
                wallPush = false;
            }
            // CameraFollow.get.snapToBehind();
        }



    }

    //get state of player, values and input
    void Update()
    {


        if (transform.position.y < -15f) { GetComponent<Health>().currentHealth -= 1; transform.position = lastSafeLocation + new Vector3(0f,1f,0f); rigid.velocity = Vector3.zero; }

        CameraFollow.get.target = transform;
        //stops rigidbody "sleeping" if we don't move, which would stop collision detection
        rigid.WakeUp();
        //handle jumping
        JumpCalculations();

        curWallJumpTime -= Time.deltaTime;
        //adjust movement values if we're in the air or on the ground
        if (!IsWallPushing())
        {
            curAccel = (grounded) ? accel : ((curWallJumpTime > 0f) ? wallJumpAirAccel : airAccel);
        }
        curRotateSpeed = (grounded) ? rotateSpeed : airRotateSpeed;

        //get movement axis relative to camera
        screenMovementSpace = Quaternion.Euler(0, mainCam.eulerAngles.y, 0);
        screenMovementForward = screenMovementSpace * Vector3.forward;
        screenMovementRight = screenMovementSpace * Vector3.right;

        //get movement input, set direction to move in
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        //only apply vertical input to movemement, if player is not sidescroller
        if (!sidescroller)
            direction = (screenMovementForward * v) + (screenMovementRight * h);
        else
            direction = Vector3.right * h;
        moveDirection = transform.position + direction;
    }

    //apply correct player movement (fixedUpdate for physics calculations)
    void FixedUpdate()
    {
        //are we grounded
        grounded = IsGrounded();
        wallRunCD -= Time.deltaTime;
        if (IsWallPushing())
        {
            wallJumpCoyote = 0.1f;
            CameraFollow.get.wallRunSnap = true;
        }

        else
        {
            wallJumpCoyote -= Time.deltaTime;
           CameraFollow.get.wallRunSnap = false;
        }

        if (!grounded)
        {
            WallRunCalculations();
            if (!wallPush)
            {
                if (Input.GetButtonUp("Jump") && rigid.velocity.y > 0 & !wallJumped)
                {
                    shorthop = true;
                    rigid.velocity = new Vector3(rigid.velocity.x, Mathf.Min(rigid.velocity.y, shorthopSpeed), rigid.velocity.z);
                }
                rigid.velocity = new Vector3(rigid.velocity.x,
                    Mathf.Max(IsWallPushing() ? -wallSlideTerminalVelocity : -terminalVelocity, rigid.velocity.y - ((shorthop ? shorthopGravity : gravityScale) * Time.deltaTime)),
                    rigid.velocity.z);
            }
        }

        if (wallPush)
        {
            rigid.velocity = new Vector3(0, rigid.velocity.y, 0);
            moveDirection = transform.position + new Vector3(wallPushDir.x, 0, wallPushDir.z);
        }

        //move, rotate, manage speed
        characterMotor.MoveTo(moveDirection, curAccel, 0.7f, true);
        if (rotateSpeed != 0 && direction.magnitude != 0)
            characterMotor.RotateToDirection(moveDirection, curRotateSpeed * 5, true);
        //characterMotor.ManageSpeed(curDecel, maxSpeed + movingObjSpeed.magnitude, true);
        manageSpeed();


        //set animation values
        if (animator)
        {
            animator.SetFloat("DistanceToTarget", characterMotor.DistanceToTarget);
            animator.SetBool("Grounded", grounded);
            animator.SetFloat("YVelocity", GetComponent<Rigidbody>().velocity.y);
        }
    }

    //prevents rigidbody from sliding down slight slopes (read notes in characterMotor class for more info on friction)
    void OnCollisionStay(Collision other)
    {
        if(other.collider.tag == "DeathZone")
        {
            GetComponent<Health>().currentHealth -= 1; transform.position = lastSafeLocation + new Vector3(0f, 1f, 0f); rigid.velocity = Vector3.zero;
        }
        //only stop movement on slight slopes if we aren't being touched by anything else
        if (other.collider.tag != "Untagged" || grounded == false)
            return;
        //if no movement should be happening, stop player moving in Z/X axis
        if (direction.magnitude == 0 && slope < slopeLimit && rigid.velocity.magnitude < 2)
        {
            //it's usually not a good idea to alter a rigidbodies velocity every frame
            //but this is the cleanest way i could think of, and we have a lot of checks beforehand, so it should be ok
            rigid.velocity = Vector3.zero;
        }
    }

    // MaxSpeed only applies to X and Z movement. Deceleration is only applied when there's no input.
    void manageSpeed()
    {

        Vector2 horSpeed = new Vector2(rigid.velocity.x, rigid.velocity.z);
        if (direction.magnitude == 0)
        {
            curDecel = (grounded) ? decel : airDecel;
        }
        else { curDecel = 0; }

        horSpeed = horSpeed.normalized * Mathf.Min(maxSpeed, horSpeed.magnitude - curDecel * Time.deltaTime);
        rigid.velocity = new Vector3(horSpeed.x, rigid.velocity.y, horSpeed.y);

    }


    private bool IsWallPushing()
    {
        //get distance to wall, from centre of collider
        float dist = GetComponent<Collider>().bounds.extents.z;
        RaycastHit hit;

        if (!wallPush)
        {
            if (grounded
                || direction == Vector3.zero
                || Vector2.Dot(new Vector2(rigid.velocity.x, rigid.velocity.z), new Vector2(direction.x, direction.z)) < 0)
            {
                animator.SetBool("WallRunning", false);
                return false;
            }
        }
        else
        {
            direction = wallPushDir;
        }
        if (Physics.Raycast(GetComponent<Collider>().bounds.center, direction, out hit, dist + 0.1f))
        {
            if (!hit.transform.GetComponent<Collider>().isTrigger)
            {

                wallPushDir = -hit.normal;
                slope = Vector3.Angle(hit.normal, Vector3.up);
                //Debug.Log(slope);
                animator.SetBool("WallRunning", true);
                return true;
            }
        }
        animator.SetBool("WallRunning", false);
        return false;
    }

    //returns whether we are on the ground or not
    //also: bouncing on enemies, keeping player on moving platforms and slope checking
    private bool IsGrounded()
    {
        //get distance to ground, from centre of collider (where floorcheckers should be)
        float dist = GetComponent<Collider>().bounds.extents.y;
        bool hitAll = true;
        foreach (Transform check in floorCheckers)
        {
            
            RaycastHit hit;
            if (Physics.Raycast(check.position, Vector3.down, out hit, dist + 0.05f))
            {
                if (hit.transform.GetComponent<Collider>().isTrigger || hit.transform.tag == "Enemy" || hit.transform.tag == "MovingPlatform" || hit.transform.tag == "Pushable" || hit.transform.tag == "DeathZone") 
                {
                    hitAll = false; break;
                    
                }
            }
            else { hitAll = false; break; }
        }
        if (hitAll) { lastSafeLocation = transform.position; }
        //check whats at players feet, at each floorcheckers position
        foreach (Transform check in floorCheckers)
        {
            RaycastHit hit;
            if (Physics.Raycast(check.position, Vector3.down, out hit, dist + 0.05f))
            {
                if (!hit.transform.GetComponent<Collider>().isTrigger)
                {
                    //slope control
                    slope = Vector3.Angle(hit.normal, Vector3.up);
                    //slide down slopes
                    if (slope > slopeLimit && hit.transform.tag != "Pushable")
                    {
                        Vector3 slide = new Vector3(0f, -slideAmount, 0f);
                        rigid.AddForce(slide, ForceMode.Force);
                    }
                    //enemy bouncing
                    if (hit.transform.tag == "Enemy" && rigid.velocity.y < 0)
                    {
                        enemyAI = hit.transform.GetComponent<EnemyAI>();
                        enemyAI.BouncedOn();
                        onEnemyBounce = 1;
                        dealDamage.Attack(hit.transform.gameObject, 1, 0f, 0f);
                    }
                    else
                        onEnemyBounce = 0;
                    //moving platforms
                    if (hit.transform.tag == "MovingPlatform" || hit.transform.tag == "Pushable")
                    {
                        movingObjSpeed = hit.transform.GetComponent<Rigidbody>().velocity;
                        movingObjSpeed.y = 0f;
                        //9.5f is a magic number, if youre not moving properly on platforms, experiment with this number
                        rigid.AddForce(movingObjSpeed * movingPlatformFriction * Time.fixedDeltaTime, ForceMode.VelocityChange);
                    }
                    else
                    {
                       
                        movingObjSpeed = Vector3.zero;
                    }
                    //yes our feet are on something
                    
                    return true;
                }
            }
        }
        movingObjSpeed = Vector3.zero;
        //no none of the floorchecks hit anything, we must be in the air (or water)
        return false;
    }

    //jumping
    private void JumpCalculations()
    {
        //keep how long we have been on the ground
        groundedCount = (grounded) ? groundedCount += Time.deltaTime : 0f;

        //play landing sound
        if (groundedCount < 0.25 && groundedCount != 0 && !GetComponent<AudioSource>().isPlaying && landSound && GetComponent<Rigidbody>().velocity.y < 1)
        {
            aSource.volume = Mathf.Abs(GetComponent<Rigidbody>().velocity.y) / 40;
            aSource.clip = landSound;
            aSource.Play();
        }
        //if we press jump in the air, save the time
        if (Input.GetButtonDown("Jump") && !grounded)
            airPressTime = Time.time;


        //if were on ground within slope limit
        if (grounded && slope < slopeLimit)
        {
            //and we press jump, or we pressed jump justt before hitting the ground
            if (Input.GetButtonDown("Jump") || airPressTime + jumpLeniancy > Time.time)
            {

                Jump(jumpForce);
            }
        }
        else if (IsWallPushing() || wallJumpCoyote > 0f)
        {
            if (Input.GetButtonDown("Jump"))
            {
                WallJump();
            }
        }
    }

    public void WallJump()
    {
        if (jumpSound)
        {
            aSource.volume = 1;
            aSource.clip = jumpSound;
            aSource.Play();
        }
        wallPush = false;
        wallRunCD = 0;
        Debug.Log(wallPushDir);
        rigid.velocity = new Vector3(-wallPushDir.x * wallJumpHor, wallJumpVer, -wallPushDir.z * wallJumpHor);
        airPressTime = 0f;
        wallJumped = true;
        curWallJumpTime = wallJumpTime;
        shorthop = false;
        animator.SetBool("WallRunning", false);
    }

    //push player at jump force
    public void Jump(Vector3 jumpVelocity)
    {
        if (jumpSound)
        {
            aSource.volume = 1;
            aSource.clip = jumpSound;
            aSource.Play();
        }
        rigid.velocity = new Vector3(rigid.velocity.x, 0f, rigid.velocity.z);
        rigid.AddRelativeForce(jumpVelocity, ForceMode.Impulse);
        airPressTime = 0f;
        wallJumped = false;
        shorthop = false;
    }
}