﻿using UnityEngine;

public class CameraFollow : Triggerable
{
    public static CameraFollow get;

    public Transform subCamera;

    public Transform lookAtTarget;
	public Transform target;									//object camera will focus on and follow
	public Vector3 targetOffset =  new Vector3(0f, 3.5f, 7);	//how far back should camera be from the lookTarget
	public bool lockRotation;									//should the camera be fixed at the offset (for example: following behind the player)
	public float followSpeed = 6;								//how fast the camera moves to its intended position
	public float inputRotationSpeed = 100;						//how fast the camera rotates around lookTarget when you press the camera adjust buttons
	public bool mouseFreelook;									//should the camera be rotated with the mouse? (only if camera is not fixed)
	public float rotateDamping = 100;							//how fast camera rotates to look at target
	public GameObject waterFilter;								//object to render in front of camera when it is underwater
	public string[] avoidClippingTags;							//tags for big objects in your game, which you want to camera to try and avoid clipping with
	
	private Transform followTarget;
	private bool camColliding;

    public bool wallRunSnap;

    public float shakeFactor = 15f;

    public float targetShakeFactor = 0f;

    public override void Trigger()
    {
        if(lookAtTarget != null) { followTarget.rotation = target.rotation; targetOffset = new Vector3(0f, 4f, -9f); }

        lookAtTarget = null;
        lockRotation = false;
        
    }


    //setup objects
    void Awake()
	{
        get = this;
		followTarget = new GameObject().transform;	//create empty gameObject as camera target, this will follow and rotate around the player
		followTarget.name = "Camera Target";
        followTarget.rotation = target.rotation;
		if(waterFilter)
			waterFilter.GetComponent<Renderer>().enabled = false;
		if(!target)
			Debug.LogError("'CameraFollow script' has no target assigned to it", transform);
		
		//don't smooth rotate if were using mouselook
		if(mouseFreelook)
			rotateDamping = 0f;
	}

    //run our camera functions each frame
    void Update()
	{
        

        if (!target)
			return;
		
		SmoothFollow ();
		if(rotateDamping > 0)
			SmoothLookAt();
		else
			transform.LookAt(target.position);
	}

	//toggle waterfilter, is camera clipping walls?
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Water" && waterFilter)
			waterFilter.GetComponent<Renderer>().enabled = true;
	}
	
	//toggle waterfilter, is camera clipping walls?
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Water" && waterFilter)
			waterFilter.GetComponent<Renderer>().enabled = false;
	}
	
	//rotate smoothly toward the target
	void SmoothLookAt()
	{
		Quaternion rotation = Quaternion.LookRotation (target.position - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, rotation, rotateDamping * Time.deltaTime);
	}

    public void snapToBehind() {
        followTarget.position = target.position;
        followTarget.Translate(targetOffset, Space.Self);
        float dot = followTarget.forward.normalized.x * transform.forward.normalized.z - followTarget.forward.normalized.z * transform.forward.normalized.x;
        //float axis = Mathf.Sign(Vector2.Dot(new Vector2(followTarget.forward.x, followTarget.forward.z).normalized, new Vector2(transform.forward.x, transform.forward.z).normalized))* inputRotationSpeed;
        float axis = Mathf.Sign(dot) * -inputRotationSpeed;
        Debug.Log(axis);
        followTarget.RotateAround(target.position, Vector3.up, axis*Time.deltaTime);
        
    }
		
	//move camera smoothly toward its target
	void SmoothFollow()
	{
        shakeFactor = Mathf.Lerp(shakeFactor, targetShakeFactor, 4f * Time.deltaTime);
        subCamera.transform.localPosition = Random.insideUnitSphere * shakeFactor;


        //move the followTarget (empty gameobject created in awake) to correct position each frame
        followTarget.position = target.position;
		followTarget.Translate(targetOffset, Space.Self);
        if (lookAtTarget != null) { lockRotation = true; }
        if (lockRotation)
        {
            if (!lookAtTarget)
            {
                followTarget.rotation = target.rotation;
            }
            else { followTarget.LookAt(lookAtTarget); }
        }
		
		else if(mouseFreelook)
		{
			//mouse look
			float axisX = Input.GetAxis ("Mouse X") * inputRotationSpeed * Time.deltaTime;
			followTarget.RotateAround (target.position,Vector3.up, axisX);
			float axisY = Input.GetAxis ("Mouse Y") * inputRotationSpeed * Time.deltaTime;
			followTarget.RotateAround (target.position, transform.right, -axisY);
		}
		else
		{
            if (!wallRunSnap)
            {
                //keyboard camera rotation look
                float axis = Input.GetAxis("CamHorizontal") * inputRotationSpeed * Time.deltaTime;
                followTarget.RotateAround(target.position, Vector3.up, axis);
            }
            else {
                Vector2 followTargetForward = new Vector2(followTarget.forward.x, followTarget.forward.z);
                Vector2 playerForward = new Vector2(target.forward.x, target.forward.z);                

                float backAngleDist = Vector2.Angle(followTargetForward, playerForward);
                float rightAngleDist = Vector2.Angle(followTargetForward, playerForward.Rotate(-90f));
                float leftAngleDist = Vector2.Angle(followTargetForward, playerForward.Rotate(90f));

                followTarget.position = target.position;
                if (backAngleDist < rightAngleDist && backAngleDist < leftAngleDist)
                {
                    followTarget.rotation = target.rotation;
                }
                else if (rightAngleDist < leftAngleDist)
                {
                    followTarget.rotation = Quaternion.Euler(target.rotation.eulerAngles+new Vector3(0f,90f,0f));
                }
                else{
                    followTarget.rotation = Quaternion.Euler(target.rotation.eulerAngles + new Vector3(0f, -90f, 0f));
                }
                followTarget.Translate(targetOffset, Space.Self);
            }
		
		}


        //where should the camera be next frame?
        Vector3 nextFramePosition = Vector3.Lerp(transform.position, followTarget.position, followSpeed * Time.deltaTime);
		Vector3 direction = nextFramePosition - target.position;
		//raycast to this position
		RaycastHit hit;
		if(Physics.Raycast (target.position, direction, out hit, direction.magnitude + 0.3f))
		{
			transform.position = nextFramePosition; 			foreach(string tag in avoidClippingTags)
				if(hit.transform.tag == tag &! hit.collider.isTrigger)
					transform.position = hit.point - direction.normalized * 0.3f;
		}
		else
		{
            //otherwise, move cam to intended position
            transform.position = nextFramePosition;
        }
        
        
	}
}