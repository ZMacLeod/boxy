﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningPlatform : MonoBehaviour
{
    [SerializeField] float rotAmount;

    Rigidbody rigid;
    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 curRot = transform.rotation.eulerAngles;
        curRot += new Vector3(0f, rotAmount * Time.deltaTime, 0f);
        rigid.MoveRotation(Quaternion.Euler(curRot));
    }
}
