﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerButton : MonoBehaviour
{

    [SerializeField] List<Triggerable> list;
    bool triggered = false;
    [SerializeField] Material disabledMat;

    public void Activate()
    {
        GetComponent<MeshRenderer>().material = disabledMat;
        transform.localScale = new Vector3(1f, 0.1f, 1f);
        foreach (Triggerable t in list) { t.Trigger(); }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && triggered == false)
        {
            triggered = true;
            Activate();
        }
    }
}
