﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : Triggerable
{
    bool activated = false;
    [SerializeField] EnemyAI enemyPrefab;

    List<EnemyAI> enemiesSpawned = new List<EnemyAI>();

    [SerializeField] float enemySpawnFreq = 1f;
    float CD;
    [SerializeField] int maxEnemies;

    public override void Trigger()
    {
        activated = true;
        GetComponent<ParticleSystem>().Play();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!activated) { return; }

        CD -= Time.deltaTime;
        if (CD < 0f && enemiesSpawned.Count < maxEnemies) { CD = enemySpawnFreq;

            EnemyAI enemy = Instantiate(enemyPrefab);
            Vector3 bounds = GetComponent<ParticleSystem>().shape.scale;
            Vector3 offset = new Vector3(Random.Range(-bounds.x / 2f, bounds.x / 2f), Random.Range(0, bounds.y / 2f), 1f);
            
            enemy.transform.position = transform.position+transform.TransformVector(offset);
            enemy.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-2f, 2f), 2f, 8f),ForceMode.VelocityChange);            enemiesSpawned.Add(enemy);
        }

    }
}
