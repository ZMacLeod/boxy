﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour
{
    [SerializeField] List<Triggerable> list;

    [SerializeField] bool triggerOnce = true;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().enabled = false;
    }

    // Update is called once per frame
   public void Activate()
    {
        foreach(Triggerable t in list) { t.Trigger(); }
        if (triggerOnce) { Destroy(gameObject); }
    }
}


public class Triggerable : MonoBehaviour
{
    public virtual void Trigger()
    {

    }

}