﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    float openSpeed = 0.25f;
    [SerializeField] public bool locked;
    [SerializeField] Material lockedMat;
    [SerializeField] Material unlockedMat;

    [SerializeField] Transform leftPanel;
    [SerializeField] Transform rightPanel;

    [SerializeField] float openDist = 7f;

    [SerializeField] List<Triggerable> triggerList;

    bool opened = false;

    [SerializeField] bool stayOpen = false;


    enum State { STILL, OPEN, CLOSE, }

    State state = State.CLOSE;



    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void Activate()
    {
        foreach (Triggerable t in triggerList) { t.Trigger(); }
    }

    // Update is called once per frame
    void Update()
    {
        if (locked)
        {
            leftPanel.GetComponent<Renderer>().material = lockedMat;
            rightPanel.GetComponent<Renderer>().material = lockedMat;
            state = State.CLOSE;
        }
        else
        {
            leftPanel.GetComponent<Renderer>().material = unlockedMat;
            rightPanel.GetComponent<Renderer>().material = unlockedMat;
            if(Vector3.Distance(transform.position,PlayerMove.get.transform.position) < openDist) { state = State.OPEN;

                if (!opened)
                {
                    Activate();
                    opened = true;
                }
            } else if(!stayOpen) { state = State.CLOSE; }
        }

        if (state == State.OPEN)
        {
            leftPanel.transform.localPosition = new Vector3(Mathf.Max(-4.25f, leftPanel.transform.localPosition.x - (2.5f * Time.deltaTime / openSpeed)), leftPanel.transform.localPosition.y, leftPanel.transform.localPosition.z);
            rightPanel.transform.localPosition = new Vector3(Mathf.Min(3.25f, rightPanel.transform.localPosition.x + (2.5f * Time.deltaTime / openSpeed)), rightPanel.transform.localPosition.y, rightPanel.transform.localPosition.z);
        }else if(state == State.CLOSE)
        {
            leftPanel.transform.localPosition = new Vector3(Mathf.Min(-1.75f, leftPanel.transform.localPosition.x + (2.5f * Time.deltaTime / openSpeed)), leftPanel.transform.localPosition.y, leftPanel.transform.localPosition.z);
            rightPanel.transform.localPosition = new Vector3(Mathf.Max(0.75f, rightPanel.transform.localPosition.x - (2.5f * Time.deltaTime / openSpeed)), rightPanel.transform.localPosition.y, rightPanel.transform.localPosition.z);

        }
    }
}
