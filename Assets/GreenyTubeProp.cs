﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenyTubeProp : Triggerable
{
    [SerializeField] GameObject GreenyPropPrefab;
    [SerializeField] EnemyAI GreenyPrefab;
    [SerializeField] int GreenyPropCount = 15;

    [SerializeField] float breakDelay;
    float breakTime;
    bool activated;
    [SerializeField] Transform goo;

    void Start()
    {
        float height = GetComponent<Collider>().bounds.size.y;
        float midHeight = height / 2f;
        float radius = GetComponent<Collider>().bounds.size.x / 2f;

        height -= 1.5f;
        radius -= 0.75f;

        for (int i = 0; i < GreenyPropCount; i++) {
            GameObject obj = Instantiate(GreenyPropPrefab);
            obj.transform.parent = transform;
            Vector2 circlePos = Random.insideUnitCircle*radius;


            obj.transform.localPosition = new Vector3(circlePos.x, midHeight + Random.Range(0, height) - (height/2f), circlePos.y);
        }
    }

    public override void Trigger()
    {
        activated = true;
        breakTime = breakDelay;
    }

    void Update() {
        if (activated) { breakTime -= Time.deltaTime;
        
        if(breakTime < 0)
            {
               
                foreach(GreenyProp prop in GetComponentsInChildren<GreenyProp>())
                {
                    Destroy(prop.gameObject);
                }

                float height = GetComponent<Collider>().bounds.size.y;
                float midHeight = height / 2f;
                float radius = GetComponent<Collider>().bounds.size.x / 2f;


                for (int i = 0; i < 5; i++)
                {
                    EnemyAI enemy = Instantiate(GreenyPrefab);
                    Vector2 circlePos = Random.insideUnitCircle * radius;


                    enemy.transform.position = transform.position + new Vector3(circlePos.x, midHeight + Random.Range(0, height) - (height / 2f), circlePos.y);
                    enemy.GetComponent<Rigidbody>().AddForce((enemy.transform.position - transform.position).normalized * 10f,ForceMode.VelocityChange);
                }
                GetComponent<Renderer>().enabled = false;
                GetComponent<Collider>().enabled = false;
                Destroy(goo.gameObject);

                Destroy(this);
            }
        }
    }


}
