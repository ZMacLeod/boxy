﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWall : Triggerable
{
    // Start is called before the first frame update
    [SerializeField] DeathWallProp deathWallProp;
    [SerializeField] float width;
    [SerializeField] float height;
    [SerializeField] Transform model;

    float speed = 6f; //RUN
    bool activated;
    float startDelay = 3f;

    [SerializeField] float maxDist;
    float distTravelled;

    [SerializeField] bool adjustModelPos = true;

    void Start()
    {
        for (int a = 0; a < width; a += 2)
        {
            for (int b = 0; b < height; b += 2)
            {

                DeathWallProp obj = Instantiate(deathWallProp);
                obj.transform.parent = transform;
                obj.transform.localPosition = new Vector3(a - width / 2f, b - height / 2f, Random.Range(0f, 2f));
                obj.startPos = obj.transform.localPosition;

            }
        }
        model.transform.localScale = new Vector3(width, height, 1);
        if (adjustModelPos) { model.transform.localPosition = new Vector3(0, height / 2f, 0); }
    }

    // Update is called once per frame
    void Update()
    {
        if (activated)
        {
            startDelay -= Time.deltaTime;
            if (startDelay < 0)
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
            distTravelled += speed * Time.deltaTime;
            CameraFollow.get.targetShakeFactor = 0.1f;
            if (distTravelled > maxDist) { Destroy(gameObject); CameraFollow.get.targetShakeFactor = 0; }
        }
    }
    public override void Trigger()
    {
        base.Trigger();
        activated = true;
        CameraFollow.get.lookAtTarget = transform;
        CameraFollow.get.targetOffset = new Vector3(0f, 5f, -12f);
    }
}
