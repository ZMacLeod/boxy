﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingFloor : Triggerable
{

    bool activated = false;
    float timeBeforeCollapse = 2f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (activated) { timeBeforeCollapse -= Time.deltaTime; }
        if (timeBeforeCollapse < 0f)
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Collider>().enabled = false;
            GetComponent<ParticleSystem>().Play();
        }

    }



    public override void Trigger()
    {
        activated = true;

    }
}
