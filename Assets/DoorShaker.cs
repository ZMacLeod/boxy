﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorShaker : MonoBehaviour
{
    Vector3 startPos;
    [SerializeField] float shakeFrequency;
    float shakeTime;
    [SerializeField] float shakeAmount;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        shakeTime -= Time.deltaTime;
        if(shakeTime < 0) { transform.Translate(Vector3.forward * shakeAmount); shakeTime = shakeFrequency; }
        transform.localPosition = Vector3.Lerp(transform.localPosition, startPos, 5f * Time.deltaTime);
    }
}
