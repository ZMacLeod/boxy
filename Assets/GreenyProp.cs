﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenyProp : MonoBehaviour
{

    Vector3 startPos;

    float time;
    float yScale;
    float waveLength;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        waveLength = Random.Range(1f, 2f);
        yScale = Random.Range(0.125f, 0.5f);
        time = Random.Range(0f, waveLength);
        float scale = Random.Range(0.8f, 2f);
        transform.rotation = Quaternion.Euler(Random.Range(0, 360f), Random.Range(0, 360f), Random.Range(0, 360f));
    }

    // Update is called once per frame
    void Update()
    {

        time += Time.deltaTime;
        float offset = Mathf.Sin(time/waveLength) * yScale;
        transform.position = startPos + new Vector3(0f, offset, 0f);

    }
}
