﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLocker : Triggerable
{

    public bool lockedState;

    public override void Trigger()
    {
        GetComponent<Door>().locked = lockedState;
        lockedState = !lockedState;
    }
}
